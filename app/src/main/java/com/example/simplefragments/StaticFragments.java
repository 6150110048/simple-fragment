package com.example.simplefragments;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StaticFragments extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_static_fragments);
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_static_fragments);
            Button btnSend = (Button) findViewById(R.id.button1);
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //การติดต่อกับ UI ใน Fragment จาก Activity
                    Fragment frag = getFragmentManager().findFragmentById(R.id.fragment_one);
                    TextView txtFrag1 = (TextView) frag.getView().findViewById(R.id.textViewOne1);
                    txtFrag1.setText("Message from Activity");
                    //Send data from activity to fragment
                    Bundle bundle = new Bundle();
                    bundle.putString("edttext", "From Activity");
                    // set Fragmentclass Arguments
                    FragmentOne fragobj = new FragmentOne();
                    fragobj.setArguments(bundle);
                }
7
            });
        }
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_static_fragments, menu);
            return true;
        }
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
    }
}